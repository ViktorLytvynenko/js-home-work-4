///Theory

// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
// Для сокращения кода, чтобы не повторять одно и то же действие.

// 2. Описати своїми словами, навіщо у функцію передавати аргумент.
// Аргументы берутся из параметров функции. Их нужно задать, чтобы получить результат какой-то функции.
// 3. Що таке оператор return та як він працює всередині функції?
// Завершает выполнение функции и возвращает её значение.


/// Practice

// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:
//
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.
//
// Необов'язкове завдання підвищеної складності
//
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).


let firstNumber = Number(prompt("Please type number", ""));
let secondNumber = Number(prompt("One more please", ""));
let question = prompt("What should I do with these numbers?", "+, -, *, /");
let result;


function operation(firstNumber, secondNumber, question) {
    result = 0;
    switch (question) {
        case "+":
            result = firstNumber + secondNumber;
            break;
        case "-":
            result = firstNumber - secondNumber;
            break;
        case "*":
            result = firstNumber * secondNumber;
            break;
        case "/":
            result = firstNumber / secondNumber;
            break;
    }
    return result;
}

function oneMore() {
    while (isNaN(firstNumber) || isNaN(secondNumber) || firstNumber == "" || secondNumber == "") {
        firstNumber = Number(prompt("Please type number", firstNumber+""));
        secondNumber = Number(prompt("One more please", secondNumber+""));
        question = prompt("What should I do with these numbers?", "+, -, *, /");
    }
    console.log(operation(firstNumber, secondNumber, question));
}
oneMore()


